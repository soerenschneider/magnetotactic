.PHONY: venv
venv:
	python3 -m venv --upgrade venv
	venv/bin/pip3 install -r requirements.txt

venv-pylint: venv
	venv/bin/pip3 install pylint pylint-exit anybadge black

lint:
	venv/bin/pylint --output-format=text *.py

install:
	sh install.sh
