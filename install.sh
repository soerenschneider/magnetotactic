#!/bin/sh

set -e

BINARY_NAME=magneto
DEST=${1:-/opt/magnetotactic}

if [ 0 -ne $(id -u) ]; then
    echo "Need to be root"
    exit 1
fi

if [ ! -d ${DEST} ]; then
    mkdir ${DEST}
    cp magnetotactic.py ${DEST}/
fi

python3 -m venv ${DEST}/venv

${DEST}/venv/bin/pip3 install -r requirements.txt

cat << EOF > /usr/local/bin/${BINARY_NAME}
#!/bin/sh

${DEST}/venv/bin/python3 ${DEST}/magnetotactic.py \$@
EOF

chmod +x /usr/local/bin/${BINARY_NAME}
