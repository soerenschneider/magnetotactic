import os
import re
import shutil
import typing
import argparse
import logging
import atexit

from difflib import SequenceMatcher

import guessit
import telegram


def main() -> None:
    args = parse_args()
    loggers = list()
    loggers.append(DefaultLogger(args.log_filename))
    if args.telegram_token or args.telegram_recipient:
        loggers.append(TelegramLog(args.telegram_token, args.telegram_recipient))

    provider = FsProvider(
        source_dir=args.source, dest_dir=args.dest, dry_run=args.dry_run
    )
    loot = Magnetotactic(provider, loggers)
    loot.move()

    atexit.register(flushlogs, loggers)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="moves your loot to the right place")
    parser.add_argument(
        "-s", "--source", dest="source", action="store", required=True, help="source"
    )
    parser.add_argument(
        "-d", "--dest", dest="dest", action="store", required=True, help="destination"
    )
    parser.add_argument(
        "-f",
        "--filename",
        dest="log_filename",
        action="store",
        default=None,
        help="Telegram secret token",
    )
    parser.add_argument(
        "-t",
        "--telegram-token",
        dest="telegram_token",
        action="store",
        default=None,
        help="Telegram secret token",
    )
    parser.add_argument(
        "-r",
        "--telegram-recipient",
        dest="telegram_recipient",
        action="store",
        default=None,
        help="Telegram secret token",
    )
    parser.add_argument(
        "-n",
        "--dry-run",
        dest="dry_run",
        action="store_true",
        default=False,
        help="dry run",
    )
    return parser.parse_args()


def init_loggers(args: argparse.Namespace) -> list:
    loggers = list()
    loggers.append(DefaultLogger(args.log_filename))

    if args.telegram_token or args.telegram_recipient:
        telegram_logger = TelegramLog(args.telegram_token, args.telegram_recipient)
        loggers.append(telegram_logger)
    return loggers


def flushlogs(loggers: list):
    for logger in loggers:
        logger.flush()


class NameNormalizer:
    _year_regex = r"\(\d{4}\)"

    @staticmethod
    def _common_words(name: str) -> str:
        return name.replace("the", "")

    @staticmethod
    def _lower(name: str) -> str:
        return name.lower()

    @staticmethod
    def _remove_year(name: str) -> str:
        return re.sub(NameNormalizer._year_regex, "", name)

    @staticmethod
    def _special_chars(name: str) -> str:
        return "".join(e for e in name if e.isalnum())

    @staticmethod
    def _strip(name: str) -> str:
        return name.strip()

    @staticmethod
    def clean(name: str) -> str:
        ret = NameNormalizer._lower(name)
        ret = NameNormalizer._common_words(ret)
        ret = NameNormalizer._remove_year(ret)
        ret = NameNormalizer._special_chars(ret)
        ret = NameNormalizer._strip(ret)
        return ret


class Similarity:
    @staticmethod
    def pick_most_similar_entry(dirs: list, name: str) -> typing.Tuple:
        cleansed_name = NameNormalizer.clean(name)
        sim_max = 0
        orig_name = None
        for entry in dirs:
            cleansed_dir = NameNormalizer.clean(entry)
            sim = Similarity._compute_similarity(cleansed_dir, cleansed_name)
            if sim > sim_max:
                sim_max = sim
                orig_name = entry
        return sim_max, orig_name

    @staticmethod
    def _compute_similarity(a: str, b: str) -> float:
        # pylint: disable=invalid-name
        return SequenceMatcher(None, a, b).ratio()


class Provider:
    # pylint: disable=no-self-use
    def get_existing_shows(self) -> typing.List:
        pass

    def get_files(self) -> typing.List:
        pass

    def move(self, source: str, destination: str) -> None:
        pass


class FsProvider(Provider):
    def __init__(self, source_dir: str, dest_dir: str, dry_run=False):
        self.source_dir = source_dir
        self.dest_dir = dest_dir
        self.dry_run = dry_run

    def get_existing_shows(self) -> typing.List:
        return [
            o
            for o in os.listdir(self.dest_dir)
            if os.path.isdir(os.path.join(self.dest_dir, o))
        ]

    def get_files(self) -> typing.List:
        return [
            o
            for o in os.listdir(self.source_dir)
            if os.path.isfile(os.path.join(self.source_dir, o))
        ]

    def move(self, source: str, destination: str) -> None:
        destination_path = os.path.join(self.dest_dir, destination)
        dirname = os.path.dirname(destination_path)
        if not os.path.exists(dirname):
            logging.info("Creating dir %s", dirname)
            os.mkdir(dirname)

        source_path = os.path.join(self.source_dir, source)
        if not self.dry_run:
            shutil.move(source_path, destination_path)


class Magnetotactic:
    def __init__(self, provider: Provider, loggers: list):
        if not provider:
            raise ValueError("No provider implementation")

        self.provider = provider
        self.loggers = loggers

    def log(self, msg: str):
        for logger in self.loggers:
            logger.log(msg)

    def move(self) -> None:
        for loot in self.provider.get_files():
            components = guessit.guessit(loot)
            if components["type"] == "episode":
                title = components["title"]
                season = components["season"]
                confidence, dest = Similarity.pick_most_similar_entry(
                    self.provider.get_existing_shows(), title
                )
                dest = f"{dest}/S{season:02d}/{loot}"
                if confidence >= 0.75:
                    self.log(
                        f"Confidence {confidence}, moving TV show '{loot}' -> {dest}"
                    )
                    self.provider.move(loot, dest)
                else:
                    self.log(
                        f"Not enough confidence {confidence}, NOT moving TV show '{loot}' -> {dest}"
                    )
            elif components["type"] == "movie":
                self.log(f"Moving movie '{loot}'")
                self.provider.move(loot, loot)


class DefaultLogger:
    def __init__(self, filename=None):
        logging.basicConfig(
            level=logging.INFO,
            filename=filename,
            filemode="a",
            format="%(asctime)s [%(levelname)s] %(message)s"
        )

    def log(self, log: str) -> None:
        logging.info(log)

    def flush(self):
        pass


class TelegramLog:
    def __init__(self, token: str, recipient: str):
        if not token:
            raise ValueError("Missing telegram token")

        if not recipient:
            raise ValueError("Missing telegram recipient")

        self._logs = list()
        self._bot = telegram.Bot(token=token)
        self._recipient = recipient

    def log(self, log: str) -> None:
        self._logs.append(log)

    def flush(self):
        if self._logs:
            text = "\n".join(self._logs)
            self._bot.send_message(chat_id=self._recipient, text=text)


if __name__ == "__main__":
    main()
